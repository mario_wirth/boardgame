package com.game.core.util;

public enum Color {

    black, red, green, blue, yellow, white;
}
