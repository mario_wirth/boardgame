package com.game.core.util;

import static java.util.concurrent.TimeUnit.SECONDS;

import com.game.core.api.Player;

public class Robot implements Player {

    private Dice dice;
    private int delay;

    public Robot(Dice dice) {
        this(dice, -1);
    }

    public Robot(Dice dice, int delay) {

        if (null == dice) {
            String msg = "Dice must not be nul";
            throw new IllegalArgumentException(msg);
        }

        this.dice = dice;
        this.delay = delay;
    }

    public int play() {

        try {
            SECONDS.sleep(delay);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return dice.roll();
    }

}
