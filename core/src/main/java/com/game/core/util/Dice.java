package com.game.core.util;

public interface Dice {

    int roll();
}
