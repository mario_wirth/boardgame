package com.game.core.util;

import com.game.core.api.Character;

public class Board {

    private CircleList<Character> characters = new CircleList<Character>();

    public boolean addCharacter(Character e) {

        if (characters.contains(e)) {
            String msg = "Character " + e + " is already in play";
            throw new IllegalArgumentException(msg);
        }

        return characters.add(e);
    }

    public Character nextCharacter() {
        return characters.next();
    }

    public void removeCharacter(Character knight) {
        characters.remove(knight);
    }

    public int countCharacters() {
        return characters.size();
    }

    public void clear() {
        characters = new CircleList<Character>();
    }

}
