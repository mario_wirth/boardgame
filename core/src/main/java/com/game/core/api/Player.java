package com.game.core.api;

public interface Player {

    int play();
}
