package com.game.core.api;

import java.util.List;



public interface EventListener {

    void gameStarted(List<Character> characters);

    void nextMove(Character active);

    void moveFinished(Character attacked, int damage);

    void gameFinished(Character winner);
}
