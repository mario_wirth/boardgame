package com.game.core.api;

import com.game.core.util.Color;

public class Knight extends Character {

    public Knight() {

        this(Color.black, 25);
    }

    public Knight(Color color, int health) {

        super("knight", color, health);
    }

}
