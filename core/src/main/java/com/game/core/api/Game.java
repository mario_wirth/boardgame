package com.game.core.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.game.core.util.Board;
import com.game.core.util.Color;
import com.game.core.util.Dice;
import com.game.core.util.EventDispatcher;
import com.game.core.util.Robot;
import com.game.core.util.SimpleDice;

public class Game extends Thread {

    public static final int CHARACTER_MAX = Color.values().length;

    public static final int CHARACTER_MIN = 2;

    public static final int HEALTH_MIN = 5;

    public static final int HEALTH_MAX = 25;

    private Map<Character, Player> players = new LinkedHashMap<Character, Player>();

    private Board board = new Board();

    private EventDispatcher dispatcher = new EventDispatcher();

    public Game() {
        // default
    }

    public Game(int players, int health, int delay) {

        setName("Game");

        Color[] colors = Color.values();
        if (colors.length < players) {
            throw new IllegalArgumentException("not enough colors");
        }

        for (int i = 0; i < players; i++) {

            Character character = new Knight(colors[i], health);
            Dice dice = new SimpleDice();
            Player robot = new Robot(dice, delay);
            add(character, robot);
        }
    }

    public Game(int players, int health) {

        this(players, health, -1);
    }

    @Override
    public void run() {

        Character winner = runGame();

        if (null == winner) {
            System.err.println("Game was interrupted.");
        }
    }

    Character runGame() {

        checkPreconditions();
        gameStarted();
        Character active = board.nextCharacter();

        while (board.countCharacters() >= CHARACTER_MIN) {

            nextMove(active);
            Character opponent = board.nextCharacter();
            Player activePlayer = players.get(active);

            int damage = opponent.takeHit(activePlayer.play());
            moveFinished(opponent, damage);

            if (opponent.isDead()) {
                board.removeCharacter(opponent);
                active = board.nextCharacter();
            } else {
                active = opponent;
            }
            if (isInterrupted()) {
                return null;
            }
        }

        gameFinished(active);
        return active;
    }

    public void add(Character c) {

        add(c, new Robot(new SimpleDice()));
    }

    public void add(Character c, Player p) {

        players.put(c, p);
        board.addCharacter(c);
    }

    public void addListener(EventListener listener) {

        dispatcher.add(listener);
    }

    private void gameStarted() {

        List<Character> allCharacters;
        allCharacters = new ArrayList<Character>(players.keySet());
        dispatcher.gameStarted(allCharacters);

    }

    private void nextMove(Character active) {

        dispatcher.nextMove(active);
    }

    private void moveFinished(Character opponent, int damage) {

        dispatcher.moveFinished(opponent, damage);
    }

    private void gameFinished(Character active) {

        dispatcher.gameFinished(active);
        cleanUp();
    }

    private void cleanUp() {

        players.clear();
        board.clear();
    }

    private void checkPreconditions() {

        if (board.countCharacters() < 2) {
            String msg = "At least two players are required for this game";
            throw new IllegalStateException(msg);
        }
    }

}
