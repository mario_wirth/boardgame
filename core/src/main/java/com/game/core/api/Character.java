package com.game.core.api;

import com.game.core.util.Color;

public abstract class Character {

    private final String type;

    private final Color color;

    private int health;

    Character(String type, Color color, int health) {

        if (null == color || health < 0) {
            String msg = "Color must not be empty and health must be greater than zero.";
            throw new IllegalArgumentException(msg);
        }

        this.type = type;
        this.color = color;
        this.health = health;
    }

    public int takeHit(int strength) {

        if (strength < 0) {
            String msg = getName() + " can't handle negative hits!";
            throw new IllegalArgumentException(msg);
        }

        if (isDead()) {
            String msg = getName() + " is already dead!";
            throw new IllegalStateException(msg);
        }

        // TODO strength dependens on character
        health -= strength;
        return strength;
    }

    public String getType() {
        return type;
    }

    public Color getColor() {
        return color;
    }

    public int getHealth() {
        return health;
    }

    public boolean isDead() {
        return 0 >= getHealth();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Character other = (Character) obj;
        if (color != other.color)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getName();
    }

    private String getName() {
        return getColor() + " " + getType();
    }

}
