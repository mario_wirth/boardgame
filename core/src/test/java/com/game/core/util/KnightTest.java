package com.game.core.util;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.game.core.api.Character;
import com.game.core.api.Knight;
import com.game.core.util.Color;

public class KnightTest {

    @Test
    public void testTakeHit() {

        Character c = new Knight(Color.red, 10);

        c.takeHit(5);
        assertFalse(c.isDead());
        assertEquals(c.getHealth(), 5);

        c.takeHit(5);
        assertTrue(c.isDead());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTakeNegativeHit() {

        Character c = new Knight(Color.yellow, 10);
        c.takeHit(-1);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testTakeHitIfAlreadyDead() {

        Character c = new Knight(Color.white, 10);
        c.takeHit(10);
        c.takeHit(1);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testInvalidName() {

        new Knight(null, 10);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testInvalidHealth() {

        new Knight(Color.blue, -10);
    }

    @Test
    public void testEquals() {

        Character darkKnight = new Knight(Color.black, 100);
        Character anotherDarkKnight = new Knight(Color.black, 99);
        assertEquals(darkKnight, anotherDarkKnight);

        Character whiteKnight = new Knight(Color.white, 100);
        assertNotEquals(darkKnight, whiteKnight);

    }

}
