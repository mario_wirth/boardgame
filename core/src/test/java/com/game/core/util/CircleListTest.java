package com.game.core.util;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.game.core.util.CircleList;

public class CircleListTest {

    @Test
    public void testNext() {

        CircleList<Integer> list = new CircleList<Integer>();

        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        for (int i = 0; i < 100; i++) {
            assertEquals((int) i % 3, (int) list.next());
        }
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testNextWhenEmpty() {
        new CircleList<Void>().next();
    }

    @Test
    public void testRemove() {

        CircleList<Integer> list = new CircleList<Integer>();

        for (int i = 1; i < 5; i++) {
            list.add(i);
        }

        assertEquals(list.size(), 4);
        assertEquals((int) list.next(), 1);

        list.remove(3);
        assertEquals((int) list.next(), 2);
        assertEquals(list.size(), 3);

        list.remove(1);
        assertEquals(list.size(), 2);
        assertEquals((int) list.next(), 4);
    }
}
