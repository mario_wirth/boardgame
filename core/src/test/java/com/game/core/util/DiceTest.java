package com.game.core.util;

import static org.testng.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import com.game.core.util.Dice;
import com.game.core.util.SimpleDice;

public class DiceTest {

    @Test
    public void testSimpleDice() {

        Dice dice = new SimpleDice();
        Set<Integer> result = new HashSet<Integer>();

        for (int i = 0; i < 100; i++) {
            result.add(dice.roll());
        }

        for (int i = 1; i <= 6; i++) {
            assertTrue(result.contains(i));
        }

    }

}
