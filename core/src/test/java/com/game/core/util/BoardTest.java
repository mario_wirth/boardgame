package com.game.core.util;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.game.core.api.Character;
import com.game.core.api.Knight;
import com.game.core.util.Board;
import com.game.core.util.Color;

public class BoardTest {

    @Test
    public void testAdd() {

        Board b = new Board();
        assertEquals(b.countCharacters(), 0);

        b.addCharacter(new Knight(Color.black, 10));
        assertEquals(b.countCharacters(), 1);

        b.addCharacter(new Knight(Color.white, 10));
        assertEquals(b.countCharacters(), 2);
    }

    @Test
    public void testRemove() {

        Board b = new Board();
        Character c = new Knight(Color.green, 10);
        b.addCharacter(c);
        assertEquals(b.countCharacters(), 1);
        assertEquals(b.nextCharacter(), c);

        b.removeCharacter(c);
        assertEquals(b.countCharacters(), 0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testUniqueCharacters() {

        Board b = new Board();
        b.addCharacter(new Knight(Color.green, 10));
        b.addCharacter(new Knight(Color.green, 3));
    }

    public void testClear() {

        Board b = new Board();
        b.addCharacter(new Knight(Color.green, 10));
        b.addCharacter(new Knight(Color.black, 3));
        assertEquals(b.countCharacters(), 2);

        b.clear();
        assertEquals(b.countCharacters(), 0);
    }

}
