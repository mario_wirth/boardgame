package com.game.console;

import static com.game.core.api.Game.CHARACTER_MAX;
import static com.game.core.api.Game.CHARACTER_MIN;
import static com.game.core.api.Game.HEALTH_MAX;
import static com.game.core.api.Game.HEALTH_MIN;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import com.game.console.DelayedPrinter.Speed;
import com.game.core.api.Game;

public class ConsoleApp {

    private static final PrintStream OUT = System.out;

    public static void main(String[] args) {

        new ConsoleApp().run();
    }

    private void run() {

        showSplashScreen();

        DelayedPrinter printer = new DelayedPrinter(OUT);
        UserInputReader r = new UserInputReader(printer, System.in);

        String name = "Number of players";
        int players = r.readInt(name, CHARACTER_MIN, CHARACTER_MAX);
        int health = r.readInt("Health", HEALTH_MIN, HEALTH_MAX);

        Game game = new Game(players, health, 2);
        game.addListener(new TextBasedEventListener(printer));
        game.start();
    }

    private void showSplashScreen() {

        String file = "splashscreen.txt";
        InputStream stream = ConsoleApp.class.getResourceAsStream(file);
        if (null == stream) {
            return;
        }

        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        DelayedPrinter p = new DelayedPrinter(OUT, Speed.fast);
        String line;
        try {
            while ((line = r.readLine()) != null) {
                p.print(line);
            }
        } catch (Exception e) {
            return;
        }
    }

}
