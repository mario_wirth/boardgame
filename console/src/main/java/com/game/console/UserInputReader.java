package com.game.console;

import java.io.InputStream;
import java.util.Scanner;

public class UserInputReader {

    private Printer printer;
    private InputStream in;

    public UserInputReader(Printer printer, InputStream in) {

        this.printer = printer;
        this.in = in;
    }

    public int readInt(String fieldName, int min, int max) {

        print(fieldName + ": ");

        while (true) {

            Scanner scanner = new Scanner(in);
            if (scanner.hasNextInt()) {
                int value = scanner.nextInt();
                if (min <= value && max >= value) {
                    return value;
                }
            }

            String msg = "Please enter a number between ";
            msg += min + " and " + max + ": ";
            print(msg);
        }
    }

    private void print(String line) {

        printer.print(line);
    }
}
