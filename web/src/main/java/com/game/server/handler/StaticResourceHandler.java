package com.game.server.handler;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URI;

import org.apache.commons.io.IOUtils;

import com.game.server.util.ContentType;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class StaticResourceHandler implements HttpHandler {

    private static final String RESOURCE_DIR = "/web";

    private static final String START_PAGE = "index.html";

    private static final String CONTENT_TYPE = "Content-type";

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        String resource = getResource(exchange.getRequestURI());
        InputStream stream = this.getClass().getResourceAsStream(resource);

        if (null != stream) {
            handleSuccess(exchange, resource, stream);
        } else {
            handleError(exchange);
        }
        exchange.close();
    }

    private void handleSuccess(HttpExchange exchange, String resource,
            InputStream stream) throws IOException {

        String type = ContentType.get(resource).toString();
        exchange.getResponseHeaders().add(CONTENT_TYPE, type);
        exchange.sendResponseHeaders(HTTP_OK, 0);

        IOUtils.copy(stream, exchange.getResponseBody());
    }

    private void handleError(HttpExchange exchange) throws IOException {

        exchange.sendResponseHeaders(HTTP_NOT_FOUND, 0);
        PrintStream out = new PrintStream(exchange.getResponseBody());
        out.println("Resource not found.");
        out.close();
    }

    private String getResource(URI uri) {

        String resource = uri.getPath();
        if ("/".equals(resource)) {
            resource += START_PAGE;
        }
        return RESOURCE_DIR + resource;
    }

}