package com.game.server.handler;

import static java.net.HttpURLConnection.HTTP_OK;

import java.io.IOException;
import java.io.PrintStream;

import com.game.console.Printer;
import com.game.console.TextBasedEventListener;
import com.game.core.api.Game;
import com.game.server.util.QueryParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class GameStartHandler implements HttpHandler {

    private static final String NUMBER_OF_PLAYER = "players";

    private static final String HEALTH = "health";

    private class SimpleStringPrinter implements Printer {

        private final StringBuffer value = new StringBuffer();

        @Override
        public void print(String line) {

            value.append(line);
            value.append("<br>");
        }

        @Override
        public String toString() {

            return value.toString();
        }
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        String query = exchange.getRequestURI().getQuery();

        QueryParser request = new QueryParser(query);

        int players = request.getInt(NUMBER_OF_PLAYER);
        int health = request.getInt(HEALTH);

        Game game = new Game(players, health);
        Printer printer = new SimpleStringPrinter();
        game.addListener(new TextBasedEventListener(printer));
        game.start();

        try {
            game.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        exchange.sendResponseHeaders(HTTP_OK, 0);
        PrintStream out = new PrintStream(exchange.getResponseBody());
        out.println(printer.toString());
        out.close();
    }
}
