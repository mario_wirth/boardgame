package com.game.server.util;

import java.util.HashMap;
import java.util.Map;

public class QueryParser {

    private Map<String, String> params;

    public QueryParser(String query) {

        this.params = parse(query);
    }

    public int getInt(String name) {

        String value = params.get(name);
        return Integer.parseInt(value);
    }

    private Map<String, String> parse(String query) {

        Map<String, String> params = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String[] splittedParam = param.split("=");
            params.put(splittedParam[0], splittedParam[1]);
        }

        return params;
    }
}
