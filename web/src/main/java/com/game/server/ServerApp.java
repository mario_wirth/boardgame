package com.game.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.game.server.handler.GameStartHandler;
import com.game.server.handler.StaticResourceHandler;
import com.sun.net.httpserver.HttpServer;

public class ServerApp {

    public static void main(String[] args) throws IOException {

        int port = 8000;

        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new StaticResourceHandler());
        server.createContext("/game/start", new GameStartHandler());
        server.start();

        System.out.println("Server is listening at port " + port);
        System.out.println("Press Enter to quit server");

        System.in.read();
        server.stop(0);
        System.out.println("Server down");
    }

}
